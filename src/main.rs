use std::fs::File;
use std::path::Path;
use serde_derive::{Deserialize, Serialize};
use serde_json;

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Gem {
    pub uid: i64,
    pub greenstone_weight: i64,
    pub water_guardian: String,
    pub vulcanic_cave: i64,
    pub sky_riddle: String,
}

// is_even - Checks an int64 to see if it's even or odd
fn is_even(num: i64) -> bool {
    // Checks to see if the remainder of num / 2 is 0 (i.e., the number is even)
    return if num % 2 == 0 {
        true
    } else {
        false
    }
}

// is_in_range - checks to see if a number is within range or not
fn is_in_range(num: i64, min: i64, max: i64 ) -> bool {
    // Checks range and returns bool
    return if num <= max && num >= min {
        true
    } else {
        false
    }
}

// is_capitalized - Checks to see if the first letter is capitalized
fn is_capitalized(string: String) -> bool {
    // Extracting first char of the string
    let first = string.chars().nth(0).unwrap();
    return if first.is_ascii_uppercase() {
        true
    } else {
        false
    }
}

// even_vowels - Checks to see if the string contains an even number of lower-case vowels
fn even_vowels(string: String) -> bool{
    // Method 1: using retain on a mutable string, to only keep the lower-case vowels
    let mut s: String = string.clone();
    s.retain(|c| c == 'a' || c == 'e' || c == 'i' || c == 'u' || c == 'o');
    // Method 2: filtering and saving the lower-case vowels from the string to a new string 'vowels'
    let vowels: Vec<char> = string.chars().filter(|c| ['a','e','i','o','u'].contains(c)).collect();

    // Checking for even number of vowels and returns bool
    return is_even(vowels.len() as i64);
    //return is_even(s.len() as i64);
}
// even_vowels - Checks to see if the string contains an even number of lower-case vowels
fn has_a(string: String) -> bool{
    // Method 1: using retain on a mutable string, to only keep the lower-case vowels
    let mut s: String = string.clone();
    s.retain(|c| c == 'a');

    return if s.len() ==2 {
        true
    } else {
        false
    }
}

// Checks to see that the length is within the allowed range
fn valid_char_range(string: String, min: i64, max: i64) -> bool {
    return is_in_range(string.len() as i64, min, max);
}

// Checks if the string ends with a dot
fn has_dot(string: String) -> bool {
    // Returns true if last char is '.'
    return if string.ends_with(".") {
        true
    } else {
        false
    }
}

// earth_validation - ensures that the weight is within 13-113 grams, and it is an even number
// Returns true if all validations passed
fn earth_validation(weight: i64) -> bool {
    // Using a counter to handle validation return value
    let mut counter = 0;

    if !is_in_range(weight,13,113) {
        println!("Greenstone Weight: outside the range");
    }else { counter+=1 }
    if !is_even(weight) {
        println!("Greenstone Weight: not even");
    }else { counter+=1 }

    // If checks passed
    return if counter == 2 {
        true
    } else {
        false
    }
}

// water_validation - checks that the name contains exactly two lower-case 'a' characters,
// and that it starts with capital letter, and is within 3 and 15 characters long (inclusive)
// Returns true if all validations passed
fn water_validation(gem: &Gem) -> bool {
    // Declaring variables, cloning the string to not move about owned string
    let name1: String = gem.water_guardian.clone();
    let name2: String = gem.water_guardian.clone();
    let name3: String = gem.water_guardian.clone();
    let mut counter = 0;

    if !is_capitalized(name1) {
        println!("Water Guardian name: not capitalized");
    }else { counter+=1 }
    if !valid_char_range(name2,3,15) {
        println!("Water Guardian name: outside the range");
    }else { counter+=1 }
    if !has_a(name3) {
        println!("Water Guardian name: missing 'a'");
    }else { counter+=1 }

    // If checks passed
    return if counter == 3 {
        true
    } else {
        false
    }
}

// fire_validation -
// Returns true if all validations passed
fn fire_validation(gem: &Gem) -> bool {
    // Using a counter for handling return value
    let mut counter = 0;

    if !is_in_range(gem.vulcanic_cave,400,700) {
        println!("Vulcanic cave temperature: outside the range");
    }else { counter+=1 }
    if !is_even(gem.vulcanic_cave) {
        println!("Vulcanic cave temperature: not even");
    }else { counter+=1 }

    // If checks passed
    return if counter == 2 {
        true
    } else {
        false
    }
}

// air_validation - checks if the riddle starts with a captial letter, and if it ends with a dot.
// The riddle should contain even number of lower-case vowels: "aouei".
// Returns true if all validations passed
fn air_validation(gem: &Gem) -> bool {
    // Declaring variables
    let riddle1: String = gem.sky_riddle.clone();
    let riddle2: String = gem.sky_riddle.clone();
    let riddle3: String = gem.sky_riddle.clone();
    let mut counter = 0;

    if !is_capitalized(riddle1) {
        println!("Sky riddle: not capitalized");
    } else { counter+=1 }
    if !even_vowels(riddle2) {
        println!("Sky riddle: not even");
    } else { counter+=1 }
    if !has_dot(riddle3) {
        println!("Water Guardian name: missing dot");
    } else { counter+=1 }

    // If checks passed
    return if counter == 3 {
        true
    } else {
        false
    }
}

// Entry point
fn main() {
    println!("Hello, world!");

    // File path variable, from project root
    let json_file_path = Path::new("lib/test-data.json");
    // Opening file
    let file = File::open(json_file_path).expect("file should open read only");
    // Deserializing using serde_json
    let elem_gems:Vec<Gem> = serde_json::from_reader(file).expect("couldn't read or parse JSON");

    // Iterating through the vector of gems elem_gem
    for gem in elem_gems.iter(){
        // Printing gem uid
        println!("\n\nGem {}", gem.uid);
        // Running checks on each gem here
        earth_validation(gem.greenstone_weight);
        water_validation(gem);
        fire_validation(gem);
        air_validation(gem);

        // If all checks passed
        if earth_validation(gem.greenstone_weight) && water_validation(gem) && fire_validation(gem) && air_validation(gem) {
            println!("VALID");
        }
    }
}
