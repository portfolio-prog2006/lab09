# Lab09 - Rust

Using serde and serde-json, familiarizing ourselves with structs and parsing in Rust

## Learning objectives for Lab09, as outlined by lecturer

> ### Objectives for Lab09
> The code should be:
> - elegant
> - composable (modular)
> - easy to maintain, update, and modify
> - should avoid using `if`, `match` and `case` expressions

----------------

## The task - The Lost Treasure of Atlantis

A modular program that reads json into structs, and evaluates the member types of these to define whether they're valid and should be used as a key. Prints results to std out.

*Crates used for this program:*
- serde = "1.0.115"
- serde_derive = "1.0.115"
- serde_json = "1.0.115"

See added json file for the structs that are evaluated by the program, `test-data.json`

#### The validation criteria (turned into functions)
- It should be an even number 
- Number within a certain range (specified as input params in function signature)
- Should be a number
- Should be capitalized
- Missing 'a'
- Missing dot '.'


The following modular functions are used in the final composed functions for checking the four elemental gems:
- `is_even(...)` - Takes an input u64 and returns true or false, based on if it's an even number
- `is_in_range(...)` - Takes a u64, a min and a max number, checks if the number is within range. Returns a bool
- `is_capitalized(...)` - Takes a string, checks if its first char is capitalized. Returns a bool
- `even_vowels(...)` - Takes a string, checks if there's an even amount of vowels in the string. Returns a bool
- `has_a(...)` - Takes a string, checks if there's an 'a' in it. Returns a bool
- `valid_char_range(...)` - Takes a string and two u64's (min and max), checks if the amount of chars is between min and max. Returns a bool
- `has_dot(...)` - Takes a string, checks if the string contains a dot ('.'). Returns a bool

#### The gem validations:
- `earth_validation(...)` - Takes a gem struct, checks that the weight is within 13-113 grams, and it is an even number
- `water_validation(...)` - Takes a gem struct, checks that the name contains exactly two lower-case 'a' characters, and that it starts with capital letter, and is within 3 and 15 characters long (inclusive)
- `fire_validation(...)` - Takes a gem struct, checks that the temperature is within 400-700 degrees Celcius, and that it is an even number
- `air_validation(...)` - Takes a gem struct, checks if the riddle starts with a captial letter, and if it ends with a dot. The riddle should contain even number of lower-case vowels: "aouei"


### *Output in main when running the program*
```
Gem 1
Water Guardian name: missing 'a'
Water Guardian name: missing 'a'


Gem 11
Greenstone Weight: outside the range
Greenstone Weight: not even
Water Guardian name: not capitalized
Water Guardian name: missing 'a'
Vulcanic cave temperature: outside the range
Sky riddle: not capitalized
Water Guardian name: missing dot
Greenstone Weight: outside the range
Greenstone Weight: not even


Gem 3
Water Guardian name: missing 'a'
Water Guardian name: missing 'a'


Gem 44
Greenstone Weight: not even
Water Guardian name: missing 'a'
Vulcanic cave temperature: outside the range
Sky riddle: not even
Water Guardian name: missing dot
Greenstone Weight: not even


Gem 5
Water Guardian name: missing 'a'
Sky riddle: not capitalized
Sky riddle: not even
Water Guardian name: missing dot
Water Guardian name: missing 'a'


Gem 12
Water Guardian name: missing 'a'
Water Guardian name: missing dot
Water Guardian name: missing 'a'


Gem 7
Greenstone Weight: outside the range
Greenstone Weight: not even
Water Guardian name: missing 'a'
Sky riddle: not even
Water Guardian name: missing dot
Greenstone Weight: outside the range
Greenstone Weight: not even


Gem 13
VALID

Gem 9
Water Guardian name: not capitalized
Water Guardian name: missing 'a'
Sky riddle: not even
Water Guardian name: not capitalized
Water Guardian name: missing 'a'


Gem 10
VALID
```
